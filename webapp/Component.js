jQuery.sap.declare("ZZX_FI_CUST_BOOK_SKUPIENP.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.generic.app.AppComponent.extend("ZZX_FI_CUST_BOOK_SKUPIENP.Component", {
	metadata: {
		"manifest": "json"
	}
});